#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#define N 200
int main()
{
	int sum, dlin, i, r, count, index, sort[N];
	char str[N], mas[N][N];
	srand(time(0));
	FILE *fp1, *fp2;
	fp1 = fopen("text1.txt", "rt");
	fp2 = fopen("text2.txt", "wt");
	for (sum, dlin; (fgets(str, N, fp1)); sum=0, dlin=0)
	{
		for (i = 0; i < strlen(str); i++)
		{
			if ((str[i] == ' ') || (str[i] == '\n'))
			{
				mas[sum][dlin] = 0;
				dlin = 0;
				sum++;
			}
			else
			{
			    mas[sum][dlin] = str[i];
				dlin++;
			}
		}
		for (count = 0; sum > count; dlin = 0)
		{
			r=rand() % sum;
			for (index = 0; (count >= index) && (dlin == 0); index++)
			{
				if (r == sort[index])
				dlin = 1;
			}
			if (dlin == 0)
			{
				fprintf(fp2, "%s ", mas[r]);
				count++;
				sort[count] = r;
			}
		}
		fprintf(fp2,"\n");
	}
	fclose(fp1);
	fclose(fp2);
	return 0;
}
